<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="text-center">
    <h1>Calculator</h1>
</div>

<div class="container">
    <div class="half-width">
        <div class="row">
            <div class="col-9">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Result</span>
                    </div>
                    <input class="form-control text-right" readonly id="result"/>
                    <div class="input-group-append">
                        <span class="input-group-text" id="previous_action_symbol"></span>
                    </div>
                    <input type="hidden" id="previous_action"/>

                </div>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-danger" id="reset_all">CE</button>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-9">
                <input class="form-control text-right" readonly id="current"/>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-danger" id="reset">C</button>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="7">7</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="8">8</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="9">9</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-primary calc-action" data-action="division">/</button>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="4">4</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="5">5</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="6">6</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-primary calc-action" data-action="multiply">*</button>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="1">1</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="2">2</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="3">3</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-primary calc-action" data-action="subtract">-</button>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="-">+/-</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value="0">0</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-info calc-symbol" data-value=".">.</button>
            </div>
            <div class="col-3">
                <button class="btn btn-block btn-primary calc-action" data-action="add">+</button>
            </div>
        </div>
        <div class="row mt-2">

            <div class="col-12">
                <button class="btn btn-block btn-success calc-action" data-action="equal">=</button>
            </div>
        </div>
    </div>
</div>
