<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('.btn.calc-symbol').on('click', function () {
            let current_value = $("#current").val();
            let number_symbol = $(this).data('value');
            if (number_symbol != "-") {
                if (number_symbol == ".") {
                    if (current_value.indexOf(".") == -1) {
                        current_value = current_value + $(this).data('value');
                    }
                } else {
                    current_value = current_value + $(this).data('value');
                }
            } else {
                current_value = current_value * -1;
            }
            $("#current").val(current_value);
        });
        $('#reset_all').on('click', function () {
            $("#current").val('');

            $("#previous_action").val('');
            $("#result").val('');
            $("#previous_action_symbol").text('');
        });
        $('#reset').on('click', function () {
            $("#current").val('');
        });
        $('.btn.calc-action').on('click', function () {
            let current_number = $("#current").val();
            let new_action = $(this).data('action');
            let previous_action = $("#previous_action").val();
            let previous_number = $("#result").val();
            let previous_action_symbol = $(this).text();
            if (current_number !== '') {

                $.ajax({
                    url: "/calculator/calculate",
                    method: "post",
                    data: {
                        current_number: current_number,
                        previous_number: previous_number,
                        new_action: new_action,
                        previous_action: previous_action
                    }
                }).done(function (response) {
                    $("#previous_action").val(response.previous_action);
                    $("#result").val(response.result);
                    $("#previous_action_symbol").text(previous_action_symbol);
                    $("#current").val('');
                });
            } else {
                $("#previous_action").val(new_action);
                $("#previous_action_symbol").text(previous_action_symbol);
            }
        });
    });
</script>
</body>
</html>
