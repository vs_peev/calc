<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculator extends CI_Controller {

    public function index() {
        $this->load->view('header');
        $this->load->view('calculator');
        $this->load->view('footer');
    }

    public function calculate() {
        if ($this->input->is_ajax_request()) {
            $this->load->model('calculate_model');
            $result = $this->calculate_model->calculate();
            header('Content-Type: application/json');
            echo json_encode($result);
        } else {
            redirect('/');
        }
    }
}
