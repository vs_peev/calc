<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calculate_model extends CI_Model {

    public function calculate() {
        $previous_action = $this->input->post('previous_action');
        $new_action = $this->input->post('new_action');
        $current_number = $this->input->post('current_number');
        $previous_number = $this->input->post('previous_number');
        $action = !empty($previous_action) ? $previous_action : 'default_action';
        return array(
            'result' => $this->$action($previous_number, $current_number),
            'previous_action' => $new_action,
        );
    }

    protected function default_action($previous_number, $current_number) {
        return $current_number;
    }

    protected function division($previous_number, $current_number) {
        return $previous_number / $current_number;
    }

    protected function multiply($previous_number, $current_number) {
        return $previous_number * $current_number;
    }

    protected function subtract($previous_number, $current_number) {
        return $previous_number - $current_number;
    }

    protected function add($previous_number, $current_number) {
        return $previous_number + $current_number;
    }
    protected function equal($previous_number, $current_number) {
        return $current_number;
    }
}